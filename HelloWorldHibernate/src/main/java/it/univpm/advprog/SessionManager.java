package it.univpm.advprog;

import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public class SessionManager implements AutoCloseable {
	
	protected static SessionFactory sessionFactory;

	protected void setup() {
		final StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
				.configure() // configures settings from hibernate.cfg.xml
				.build();
		try {
			sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
		} catch (Exception ex) {
			ex.printStackTrace();
			StandardServiceRegistryBuilder.destroy(registry);
		}
	}

	
	public SessionManager() {
		if (sessionFactory == null) {
			setup();
		}
	}
	
	@Override
	public void close() {
		if (sessionFactory != null) {
			sessionFactory.close();
		}
	}
	
	public static SessionFactory getSessionFactory() {
		SessionManager sm = new SessionManager();
		
		assert sm.sessionFactory != null;
		return sm.sessionFactory;
	}
}
