package it.univpm.advprog;
import javax.persistence.*;

@Entity
@Table(name = "book")
public class Book {
	
	protected long id;
	
	protected String title;
	
	protected Author author;
	
	protected String isbn;
	protected float price;
	
    public Book() {}
	
    @Id
    @Column(name = "book_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long getId() {
		return id;
	}
	
    public void setId(long id) {
    	this.id = id;	
    }
	
    @Column( unique = true )
    public String getTitle() {
    	return title;
    }
    
    public void setTitle(String title) {
    	this.title = title;
    }
    
	@ManyToOne(fetch=FetchType.LAZY, targetEntity = Author.class)
    public Author getAuthor() {
    	return author;
	}

    public void setAuthor(Author author) {
		this.author = author;
    }
    
    @Column
    public float getPrice() {

    	return price;
	}
    
    public void setPrice(float price) {
    	this.price = price;
    }

    @Column
	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

}