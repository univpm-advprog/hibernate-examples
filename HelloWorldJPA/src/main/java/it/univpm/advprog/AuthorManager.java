package it.univpm.advprog;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class AuthorManager implements AutoCloseable {
	
    // Create an EntityManager
    private static final EntityManagerFactory ENTITY_MANAGER_FACTORY = Persistence
            .createEntityManagerFactory("puBookStore");
    
    EntityManager manager;
    EntityTransaction transaction = null;

	protected void setup() {
		this.manager = ENTITY_MANAGER_FACTORY.createEntityManager();
	}

	public AuthorManager() {
		setup();		
	}
	
	@Override
	public void close() {
		this.manager.close();
	}

	
	public long create(String firstName, String lastName, Date birthDate) {
		
		long id = -1;
		Author author = new Author();
		author.setFirstName(firstName);
		author.setLastName(lastName);
		author.setBirthDate(birthDate);
		
		try {
			transaction = manager.getTransaction();
			
			transaction.begin();
			
			manager.persist(author);
			
			transaction.commit();
			
			id = author.getId();
			
		} catch (Exception ex) {
            // If there are any exceptions, roll back the changes
            if (transaction != null) {
                transaction.rollback();
            }
            // Print the Exception
            ex.printStackTrace();
			
		} 
		
		return id;
	}
	
	public Author read(long authorId) {
		Author author = null;
		
		try {
			transaction = manager.getTransaction();
			
			transaction.begin();
			
			List<Author> authors = (List<Author>) manager.createQuery("SELECT a FROM Author a WHERE id = :myID").setParameter("myID", authorId).getResultList();
			
			if (authors.size() == 0) {
				throw new Exception("Author with ID " + authorId + " not present");
				
			} else if (authors.size() > 1) {
				
				throw new Exception("Too many authors with ID " + authorId + ": " + authors.size());
			}
			
			
			author = authors.get(0);
			
			System.out.println("First name: " + author.getFirstName());
			System.out.println("Last name: " + author.getLastName());
			System.out.println("Birth date: " + author.getBirthDate());
			

			transaction.commit();
			

		} catch (Exception ex) {
            // If there are any exceptions, roll back the changes
            if (transaction != null) {
                transaction.rollback();
            }
            // Print the Exception
            ex.printStackTrace();
			
		} 

		return author;


	}
	
	public void update(long id, String firstName, String lastName, Date birthDate) {
		


		Author author = new Author();
		author.setId(id);
		author.setFirstName(firstName);
		author.setLastName(lastName);
		author.setBirthDate(birthDate);
		
		this.update(author);
	}
	
	public void update(Author author) {
		

		try {
			transaction = manager.getTransaction();
			
			transaction.begin();

			manager.merge(author);
			transaction.commit();
			
		} catch (Exception ex) {
            // If there are any exceptions, roll back the changes
            if (transaction != null) {
                transaction.rollback();
            }
            // Print the Exception
            ex.printStackTrace();
			
		} 


	}
	
	public void delete(long authorId) {

		Author author = new Author();
		author.setId(authorId);
		
		this.delete(author);
	}
	
	public void delete(Author author) {
		
		try {
			transaction = manager.getTransaction();
			
			transaction.begin();

			if (! manager.contains(author))
			{
				author = manager.merge(author);
				//throw new RuntimeException("Must pass a persistent entity");
			}
			manager.remove(author);
			transaction.commit();
			
		} catch (Exception ex) {
            // If there are any exceptions, roll back the changes
            if (transaction != null) {
                transaction.rollback();
            }
            // Print the Exception
            ex.printStackTrace();
			
		} 


	}
	
	public Author getOrCreate(String firstName, String lastName) {

		Author found = null;
		
		try {
			transaction = manager.getTransaction();
			
			transaction.begin();

			List<Author> authors = (List<Author>) manager.createQuery("SELECT a FROM Author a WHERE firstName = '" + firstName + "' AND lastName = '" + lastName + "'").getResultList();
			
			if (authors.size() > 1) {
				// error
				throw new Exception("Too many authors with the passed parameters: " + authors.size());
			} else if (authors.size() == 1) {
				// get
				found = authors.get(0);

			} else {
				// create
				found = new Author();
				found.setFirstName(firstName);
				found.setLastName(lastName);
				manager.persist(found);
				
			}
			
			transaction.commit();
			
		} catch (Exception ex) {
            // If there are any exceptions, roll back the changes
            if (transaction != null) {
                transaction.rollback();
            }
            // Print the Exception
            ex.printStackTrace();
			
		} 

		
		return found;
	}	
	
}
