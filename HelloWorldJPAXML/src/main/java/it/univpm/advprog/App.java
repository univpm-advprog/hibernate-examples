package it.univpm.advprog;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class App {
	public static void main(String[] args) {

		try (AuthorManager aManager = new AuthorManager()) {
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
//			aManager.create("Arthur Conan", "Doyle", df.parse("1859-05-22"));
			Author author = aManager.getOrCreate("Arthur Conan", "Doyle");
			author.setBirthDate(df.parse("1859-05-22"));
			aManager.update(author);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try (BookManager bManager = new BookManager()) {
			
			Book book = bManager.create("Uno studio in rosso",  10.50f, "Arthur Conan", "Doyle");
			assert book.getId() > 0; // the instance is persistent
			
			// forza inserimento di libro con titolo duplicato
			Book book3 = bManager.create("Uno studio in rosso",  10.50f, "Arthur Conan", "Doyle");
			assert book3.getId() <= 0; // the instance is not persistent, because of duplicate title

			bManager.update(book.getId(), "Alice nel paese delle meraviglie", 21.00f,  "Lewis", "Carroll");
			
			Book book2 = bManager.create("Il segno dei quattro", 12f, "Arthur Conan", "Doyle");
			
			bManager.delete(book);
			bManager.delete(book2);
		} catch (Exception ex) {
			throw ex;
		}
		
	}

}
