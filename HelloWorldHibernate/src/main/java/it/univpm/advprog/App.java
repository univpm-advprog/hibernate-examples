package it.univpm.advprog;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class App {
	public static void main(String[] args) {

		try {
			AuthorManager aManager = new AuthorManager();
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
	//			aManager.create("Arthur Conan", "Doyle", df.parse("1859-05-22"));
			Author author = aManager.getOrCreate("Arthur Conan", "Doyle");
			author.setBirthDate(df.parse("1859-05-22"));
			aManager.update(author);
		
			BookManager bManager = new BookManager();
			Book book = bManager.create("Uno studio in rosso",  10.50f, "Arthur Conan", "Doyle");
	//			long bookId3 = bManager.create("Uno studio in rosso",  10.50f, "Arthur Conan", "Doyle");
	
			bManager.update(book.getId(), "Alice nel paese delle meraviglie", 21.00f,  "Lewis", "Carroll");
			
			Book book2 = bManager.create("Il segno dei quattro", 12f, "Arthur Conan", "Doyle");
			Book book3 = bManager.create("Il mastino di Baskerville", 13f, "Arthur Conan", "Doyle");
			
			AuthorSummary as = aManager.getAuthorSummary(author);
			System.out.println("author: " + author + " - # books: " + as.getNumBooks());
		
		} catch (Exception e) { 
			e.printStackTrace();
		}
//		try {
//			bManager.delete(book);
//			bManager.delete(book2);
//		} catch (Exception ex) {
//			throw ex;
//		}		
	}

}
