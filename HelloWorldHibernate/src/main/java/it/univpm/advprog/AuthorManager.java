package it.univpm.advprog;

import java.util.Date;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.criterion.Restrictions;


public class AuthorManager {
	
	protected SessionFactory sessionFactory = SessionManager.getSessionFactory();

	
	public long create(String firstName, String lastName, Date birthDate) {
		
		assert sessionFactory != null;
		
		Author author = new Author();
		author.setFirstName(firstName);
		author.setLastName(lastName);
		author.setBirthDate(birthDate);
		
		try (Session session = sessionFactory.openSession()) {
			session.beginTransaction();
	
			session.save(author);
	
			session.getTransaction().commit();
		}catch (Exception ex) {
			throw ex;
		}

		
		return author.getId();
	}
	
	public Author read(long authorId) {
		assert sessionFactory != null;
		
		try (Session session = sessionFactory.openSession()) {

		//		long bookId = 20;
			Author author = session.get(Author.class, authorId);
		
			System.out.println("First name: " + author.getFirstName());
			System.out.println("Last name: " + author.getLastName());
			System.out.println("Birth date: " + author.getBirthDate());
			
			return author;

		}catch (Exception ex) {
			throw ex;
		}


	}
	
	public void update(long id, String firstName, String lastName, Date birthDate) {
				
		Author author = new Author();
		author.setId(id);
		author.setFirstName(firstName);
		author.setLastName(lastName);
		author.setBirthDate(birthDate);
		
		this.update(author);
	}
	
//	public void update(long id, String firstName, String lastName, Date birthDate) {
	public void update(Author author) {
		assert sessionFactory != null;
		
		try (Session session = sessionFactory.openSession()) {
			session.beginTransaction();
	
			session.update(author);
	
			session.getTransaction().commit();
		} catch (Exception ex) {
			throw ex;
		}


	}
	
	public void delete(long authorId) {

		Author author = new Author();
		author.setId(authorId);
		
		this.delete(author);
	}

	
	public void delete(Author author) {
		
		assert sessionFactory != null;
		
		try (Session session = sessionFactory.openSession()) {
			session.beginTransaction();
	
			session.delete(author);
	
			session.getTransaction().commit();
		} catch (Exception ex) {
			throw ex;
		}


	}
	
	public Author getOrCreate(String firstName, String lastName) {

		Author found = null;
		
		assert sessionFactory != null;
		
		try (Session session = sessionFactory.openSession()) {
			session.beginTransaction();
			
			try {
				TypedQuery<Author> query = session.createQuery("SELECT a FROM Author a WHERE a.firstName = :firstName AND a.lastName = :lastName", Author.class);
				found = query.setParameter("firstName", firstName).setParameter("lastName", lastName).getSingleResult();
			
			} catch (NoResultException ex)
			{
				found = new Author();
				found.setFirstName(firstName);
				found.setLastName(lastName);
				
				session.save(found);
			}	
			finally {
				session.getTransaction().commit();
			}
		} catch (Exception ex) {
			throw ex;
		}

		
		return found;
	}	
	
	public AuthorSummary getAuthorSummary(Author author) {
		assert sessionFactory != null;

		AuthorSummary as = null;
		
		try (Session session = sessionFactory.openSession()) {
			session.beginTransaction();

			as = session.get(AuthorSummary.class, author.getId());
			session.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		return as;
	}
	
}
