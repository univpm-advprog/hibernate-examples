package it.univpm.advprog;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.annotations.Immutable;
import org.hibernate.annotations.Subselect;
import org.hibernate.annotations.Synchronize;

@Entity
@Immutable
@Subselect(
		value="SELECT a.id as id, COUNT(b.author_id) as numBooks FROM author a LEFT JOIN book b ON b.author_id = a.id GROUP BY a.id"
		)
@Synchronize({ "Author", "Book" })
public class AuthorSummary {
	
	@Id
	protected long id;
		
	protected long numBooks;

	
	public long getNumBooks() {
		return this.numBooks;
	}
}
