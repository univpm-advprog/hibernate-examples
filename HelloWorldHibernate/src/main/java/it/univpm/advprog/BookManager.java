package it.univpm.advprog;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class BookManager  {
	protected SessionFactory sessionFactory = SessionManager.getSessionFactory();


	public Book create(String title, float price, String authorFirstName, String authorLastName) {
		assert sessionFactory != null;
		
		Book book = new Book();
		book.setTitle(title);
		
		AuthorManager am = new AuthorManager();
		Author a = am.getOrCreate(authorFirstName, authorLastName);
		book.setAuthor(a);
		
		book.setPrice(price);

		try (Session session = sessionFactory.openSession()) {
			session.beginTransaction();
	
			session.save(book);
	
			session.getTransaction().commit();
		}catch (Exception ex) {
			throw ex;
		}
		
		return book;
	}

	public Book read(long bookId) {
		assert(sessionFactory != null);
		
		try (Session session = sessionFactory.openSession()) {
			Book book = session.get(Book.class, bookId);
			
			System.out.println("Title: " + book.getTitle());
			System.out.println("Author: " + book.getAuthor());
			System.out.println("Price: " + book.getPrice());
			
			return book;
		}catch (Exception ex) {
			throw ex;
		}
		
	}

	public void update(long bookId, String new_title, float new_price, String authorFirstName, String authorLastName) {
		Book book = new Book();
		book.setId(bookId);
		book.setTitle(new_title);
		book.setPrice(new_price);
		
		this.update(book, authorFirstName, authorLastName);
	}
	
	public void update(Book book, String authorFirstName, String authorLastName) {
		assert(sessionFactory != null);
		
		
		try {
			AuthorManager am = new AuthorManager();
			Author author = am.getOrCreate(authorFirstName, authorLastName);
			book.setAuthor(author);
		} catch (Exception ex) {
			throw ex;
		}

		try (Session session = sessionFactory.openSession()) {
			session.beginTransaction();
	
			session.update(book);
	
			session.getTransaction().commit();
		}catch (Exception ex) {
			throw ex;
		}

	}

	public void delete(long bookId) {
		Book book = new Book();
		book.setId(bookId);

		this.delete(book);
	}
	
	public void delete(Book book) {
		assert(sessionFactory != null);
		
		try (Session session = sessionFactory.openSession()) {
			session.beginTransaction();
	
			session.delete(book);
	
			session.getTransaction().commit();
		}catch (Exception ex) {
			throw ex;
		}

	}
	
	public Book getOrCreate(String title) {
		assert(sessionFactory != null);

		Book found = null;
		
		try (Session session = sessionFactory.openSession()) {
			session.beginTransaction();
			
			try {
				TypedQuery<Book> query = session.createQuery("SELECT b FROM Book b WHERE b.title = :title", Book.class);
				found = query.setParameter("title", title).getSingleResult();
			
			} catch (NoResultException ex)
			{
				found = new Book();
				found.setTitle(title);
				
				session.save(found);

			}	
			session.getTransaction().commit();

		} catch (Exception ex) {
			throw ex;
		}

		
		return found;
	}

}